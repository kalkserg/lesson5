package lesson5;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class User extends Person {
    private Date dateOfBirth;
    private List<Book> myListBooks = new ArrayList<>();

    public User(String firstName, String lastName, String fatherName, Date dateOfBirth) {
        super(firstName,lastName,fatherName);
        this.dateOfBirth = dateOfBirth;
    }

    public Date getDateOfBirth() {
        return new Date(dateOfBirth.getTime());
    }

    public List<Book> getMyListBooks() {
        return myListBooks;
    }

    public String showMyListBooks() {
        StringBuffer str = new StringBuffer();
        str.append(getFirstName());
        str.append(" ");
        str.append(getLastName());
        if(!myListBooks.isEmpty()) {
            str.append(" на руках ");
            for (Book item : myListBooks) {
                str.append(item.getBookName());
                str.append(" ");
                str.append(item.getAuthorNickName());
                str.append(", ");
            }
            str.append("\n");
            return str.toString();
        }
        return str.toString() + " нет книг на руках";
    }

    public void getBook(Book book) {
        myListBooks.add(book);
    }

    public void returnBook(Book book) {
        myListBooks.remove(book);
    }

    @Override
    public String toString() {
        return  "User{" +
                " Name='" + getFirstName() + '\'' +
                ", lastName='" + getLastName() + '\'' +
                ", lastName='" + getFatherName() + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        if(super.equals(user) == false) return false;
        if(dateOfBirth.getDate() != user.getDateOfBirth().getDate()) return false;
        if(dateOfBirth.getMonth() != user.getDateOfBirth().getMonth()) return false;
        if(dateOfBirth.getYear() != user.getDateOfBirth().getYear()) return false;
        return true;
    }
}
