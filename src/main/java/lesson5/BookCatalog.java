package lesson5;

import java.util.ArrayList;
import java.util.List;

public class BookCatalog implements Catalog<Book> {
    private List<Book> books = new ArrayList<>();

    @Override
    public void add(Book book) {
        books.add(book);
    }

    @Override
    public void del(Book book) {
        books.remove(book);
    }

    @Override
    public List<Book> getCatalog() {
        return books;
    }

    public boolean isRegistered(Book book){
        for (Book item:  books) {
            if(item.equals(book)) return true;
        }
        return false;
    }
}
