package lesson5;

import java.util.HashSet;
import java.util.Set;

public class AuthorCatalog implements Catalog<Author>{
    private Set<Author> authors = new HashSet<>();

    @Override
    public void add(Author author) {
        authors.add(author);
    }

    @Override
    public void del(Author author) {
        authors.remove(author);
    }

    @Override
    public Set<Author> getCatalog() {
        return authors;
    }

    public Author getAuthorByNickname(String nickName){
        for (Author item:  authors) {
            if(item.getNickName() == nickName) return item;
        }
        return null;
    }

    @Override
    public boolean isRegistered(Author author){
        if(author == null) return false;
        for (Author item:  authors) {
            if(item.equals(author)) return true;
        }
        return false;
    }

    public boolean isRegistred(String nickName){
        if(nickName == null) return false;
        if(isRegistered(getAuthorByNickname(nickName))==false) return false;
        return true;
    }
}
