package lesson5;

public class Author extends Person{
    private String nickName;

    public Author(String firstName, String lastName, String fatherName, String nickName) {
        super(firstName, lastName, fatherName);
        this.nickName = nickName;
    }

    public String getNickName() {
        return nickName;
    }

    @Override
    public String toString() {
        return "Author{" +
                " Name='" + getFirstName() + '\'' +
                ", lastName='" + getLastName() + '\'' +
                ", fatherName='" + getFatherName() + '\'' +
                ", nickName='" + nickName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Author author = (Author) o;
        if(super.equals(author) == false) return false;
        if(nickName != author.getNickName()) return false;
        return true;
    }
}
