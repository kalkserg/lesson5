package lesson5;

import java.util.HashSet;
import java.util.Set;

public class UserCatalog implements Catalog<User>{
    private Set<User> users = new HashSet<>();

    @Override
    public void add(User user) {
        users.add(user);
    }

    @Override
    public void del(User user) {
        users.remove(user);
    }

    public Set<User> getCatalog() {
        return users;
    }

    public boolean isRegistered(User user){
        for (User item:  users) {
            if(item.equals(user)) return true;
        }
        return false;
    }

}