package lesson5;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class test {
    public static void main(String[] args) throws LibraryException {
        //create librarian
        Librarian librarian = Librarian.getInstance();
        Librarian lib2 = Librarian.getInstance(); //the same librarian

        //create users
        User user1 = new User("User1", "User1", "User1", new Date(2001,1,01));
        User user2 = new User("User2", "User2", "User2", new Date(2002,2,02));
        User user3 = new User("User3", "User3", "User3", new Date(2003,3,03));

        //create authors
        Author author1 = new Author("Ivan","Ivanov","Ivanych","Ivan");
        Author author2 = new Author("Gosha","Goshko","Goshevich","Gosa");

        //create books
        Book book1 = new Book("Terminator","Ivan",2001,100);
        Book book2 = new Book("Rembo","Gosa",2002,200);
        Book book3 = new Book("Ember","Jili",2003,300);

        //add authors in catalog
        lib2.addAuthor(author1);
        lib2.addAuthor(author2);

        //add user in catalog
        try{
            lib2.addUser(user1);
        }catch (LibraryException e){
            e.printStackTrace();
        }

        try{
            lib2.addUser(user2);
        }catch (LibraryException e){
            e.printStackTrace();
        }

        try{
            lib2.addUser(user3);
        }catch (LibraryException e){
            e.printStackTrace();
        }

        System.out.println("UserCatalog");
        System.out.println(lib2.getUserCatalog().getCatalog());

        try{
            lib2.delUser(user3);
        }catch (LibraryException e){
            e.printStackTrace();
        }

        System.out.println("UserCatalog");
        System.out.println(lib2.getUserCatalog().getCatalog());


        //add books in catalog
        lib2.addBook(book1);
        lib2.addBook(book2);
        lib2.addBook(book2);
        lib2.addBook(book3);

        //give books
        try{
            lib2.giveBook(book1,user1);
        } catch(LibraryException e){
            e.printStackTrace();
        }

        try{
            lib2.giveBook(book2,user1);
        } catch(LibraryException e){
            e.printStackTrace();
        }

        try{
            lib2.giveBook(book2,user2);
        } catch(LibraryException e){
            e.printStackTrace();
        }

        System.out.println("history1");
       //librarian.showHistory();
        System.out.println(user1.showMyListBooks());
        System.out.println(user2.showMyListBooks());

        //System.out.println("history2");
        //librarian.returnBook(book2,user2);
        //librarian.showHistory();
        //System.out.println(user1.showMyListBooks());
        //System.out.println(user2.showMyListBooks());

        // search for a book by nickname
        System.out.println("Книга book3 в библиотеке - " + librarian.getBookCatalog().isRegistered(book3));
        System.out.println("Книга book2 в библиотеке - " + librarian.getBookCatalog().isRegistered(book2));
        System.out.println("Книга book3 на руках у читателя - " + librarian.searchBookFromUsers(book3));
        System.out.println("Книга book1 на руках у читателя " + librarian.searchBookFromUsers(book2));
    }
}
