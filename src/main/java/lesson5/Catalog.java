package lesson5;

import java.util.Collection;
//import java.util.Set;

public interface Catalog<T> {
    void add(T t);
    void del(T t);
    Collection<T> getCatalog();
    boolean isRegistered(T t);
}
