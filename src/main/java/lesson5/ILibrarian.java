package lesson5;

import java.util.List;

public interface ILibrarian {
    void addBook(Book book) throws LibraryException;
    void addAuthor(Author author);
    void addUser(User user) throws LibraryException;
    void delBook(Book book) throws LibraryException;
    void delAuthor(Author author) throws LibraryException;
    void delUser(User user) throws LibraryException;

    void giveBook(Book book, User user) throws LibraryException;
    void returnBook(Book book, User user);

    UserCatalog getUserCatalog();
    AuthorCatalog getAuthorCatalog();
    BookCatalog getBookCatalog();

    void showHistory();
}