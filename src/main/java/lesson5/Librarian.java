package lesson5;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public final class Librarian implements ILibrarian {

    private static Librarian librarian;
    private final AuthorCatalog authorCatalog = new AuthorCatalog();
    public BookCatalog bookCatalog = new BookCatalog();
    private final UserCatalog userCatalog = new UserCatalog();

    private final String GIVE_A_BOOK = "GIVE_A_BOOK";
    private final String RETURN_A_BOOK = "RETURN_A_BOOK";

    private Librarian() {
    }

    public static synchronized Librarian getInstance() {
        if (librarian == null)
            librarian = new Librarian();
        return librarian;
    }

    @Override
    public void addBook(Book book) {
        bookCatalog.add(book);
    }

    @Override
    public void delBook(Book book) throws LibraryException {
        if(bookCatalog.isRegistered(book)) bookCatalog.del(book);
        else {
            throw new LibraryException("Книга не зарегистрирована " + book.toString());
        }
    }

    @Override
    public void addUser(User user) throws LibraryException {
        if(userCatalog.isRegistered(user) == false) userCatalog.add(user);
        else {
            throw new LibraryException("Такой пользователь уже зарелистрирован " + user.toString());
        }
    }

    @Override
    public void delUser(User user) throws LibraryException{
        if(userCatalog.isRegistered(user)) userCatalog.del(user);
        else {
            throw new LibraryException("Пользователь не зарелистрирован " + user.toString());
        }
    }

    @Override
    public void addAuthor(Author author){
        authorCatalog.add(author);
    }

    @Override
    public void delAuthor(Author author) throws LibraryException {
        if(authorCatalog.isRegistered(author)) authorCatalog.del(author);
        else {
            throw new LibraryException("Нет такого автора " + author.toString());
        }
    }

    @Override
    public void giveBook(Book book, User user) throws LibraryException {
        if(!bookCatalog.isRegistered(book)) throw new LibraryException("Нет такой книги " + book.toString());
        else if(!userCatalog.isRegistered(user)) throw new LibraryException("Нет такого пользователя " + user.toString());
        else {
            user.getBook(book);
            bookCatalog.del(book);
            addToHistoryFile(book, user, GIVE_A_BOOK);
        }
    }

    @Override
    public void returnBook(Book book, User user){
        user.returnBook(book);
        bookCatalog.add(book);
        addToHistoryFile(book, user, RETURN_A_BOOK);
    }

    @Override
    public void showHistory() {
        try(FileReader reader = new FileReader("history.txt"))
        {
            int c;
            while((c=reader.read())!=-1){
                System.out.print((char)c);
            }
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
    }

    public Author getAuthorByNikname(String nick){
        return authorCatalog.getAuthorByNickname(nick);
    }

    public void addToHistoryFile(Book book, User user, String flag) {
        try (FileWriter writer = new FileWriter("history.txt", true)) {
            writer.write(book.toString());
            writer.write(" ");
            writer.write(user.toString());
            writer.write(" ");
            writer.write(flag);
            writer.write("\n");
            writer.flush();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public UserCatalog getUserCatalog(){
        return userCatalog;
    }
    public AuthorCatalog getAuthorCatalog(){
        return authorCatalog;
    }
    public BookCatalog getBookCatalog(){
        return bookCatalog;
    }

    public List<User> searchBookFromUsers(Book searchBook) {
        List<User> list = new ArrayList<>();
        for(User user: userCatalog.getCatalog()){
            for(Book book: user.getMyListBooks()){
                if(book.equals(searchBook)) list.add(user);
            }
        }
        return list;
    }
}
