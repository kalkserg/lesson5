package lesson5;

abstract class Person  {
    private String firstName;
    private String lastName;
    private String fatherName;

    public Person(String firstName, String lastName, String fatherName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.fatherName = fatherName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public boolean equals(Person person){
        if(firstName != person.getFirstName()) return false;
        if(lastName != person.getLastName()) return false;
        if(fatherName != person.getFatherName()) return false;
        return true;
    }
}
