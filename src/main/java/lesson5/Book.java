package lesson5;

public class Book {
    private String bookName;
    private String authorNickName;
    private int yearIssue;
    private int numOfPages;

    public Book(String bookName, String authorNickName, int yearIssue, int numOfPages) {
        this.bookName = bookName;
        this.authorNickName = authorNickName;
        this.yearIssue = yearIssue;
        this.numOfPages = numOfPages;
    }

    public String getBookName() {
        return bookName;
    }

    public String getAuthorNickName() {
        return authorNickName;
    }

    public int getYearIssue() {
        return yearIssue;
    }

    public int getNumOfPages() {
        return numOfPages;
    }

    @Override
    public String toString() {
        return "Book{" +
                "bookName='" + bookName + '\'' +
                ", authorNickName=" + authorNickName +
                ", yearIssue=" + yearIssue +
                ", numOfPages=" + numOfPages +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        if(bookName.equals(book.getBookName())==false) return false;
        if(authorNickName.equals(book.getAuthorNickName())==false) return false;
        if(yearIssue != book.getYearIssue()) return false;
        if(numOfPages != book.getNumOfPages()) return false;
        return true;
    }
}
